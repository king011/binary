declare module 'mkdir-recursive' {
    export function mkdir(path: string, callback: (err: Error | null) => void): void;
    export function mkdir(path: string, mode: number, callback: (err: Error | null) => void): void;
}
