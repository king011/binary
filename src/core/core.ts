import { readdir, stat, Stats } from 'fs';
import { join, extname } from 'path';
import { Context, Message, Type } from './type';
import { DefaultHandlers } from './handler';
import '../handler/c/c';

/**
 * 定義核心
 */
export class Core {
    ctx: Context
    handler = DefaultHandlers
    constructor() {
        this.ctx = new Context()
    }
    /**
     * 打印 註冊 符號信息
     */
    display() {
        this.ctx.display()
    }
    /**
     * 註冊一個消息
     * @param pkg 包名
     * @param name 消息名
     * @param filename 檔案名
     */
    register(pkg: string, name: string, filename: string): Message {
        return this.ctx.register(pkg, name, filename)
    }
    /**
     * 創建代碼
     * @param name 處理器名稱
     * @param dst 輸出目錄
     */
    output(name: string, dst: string): Promise<void> {
        return this.handler.handle(this.ctx, name, dst)
    }
    /**
     * 加載 定義
     * @param src 輸入檔案夾
     */
    async require(src: string): Promise<void> {
        const items = await this._find(src)
        if (!items || items.length == 0) {
            return
        }
        for (let i = 0; i < items.length; i++) {
            const str = "require('" + items[i] + "')"
            eval(str)
        }
    }
    private async  _find(src: string): Promise<Array<string>> {
        const items = new Array<string>()
        let targets = [src]
        while (true) {
            const dirs = new Array<string>()
            for (let i = 0; i < targets.length; i++) {
                const target = targets[i]
                const files: string[] = await new Promise((resolve, reject) => {
                    readdir(target, { encoding: "utf8" }, (e: Error | null, files: string[]) => {
                        if (e == null) {
                            resolve(files)
                        } else {
                            reject(e)
                        }
                    })
                })
                if (!files || files.length == 0) {
                    continue
                }
                for (let j = 0; j < files.length; j++) {
                    const file = join(target, files[j])
                    const stats: Stats = await new Promise((resolve, reject) => {
                        stat(file, (e: Error | null, stats: Stats) => {
                            if (e == null) {
                                resolve(stats)
                            } else {
                                reject(e)
                            }
                        })
                    })
                    if (stats.isFile()) {
                        if (extname(file).toLowerCase() == ".js") {
                            items.push(file)
                        }
                    } else {
                        dirs.push(file)
                    }
                }
            }
            if (dirs.length == 0) {
                break
            }
            targets = dirs
        }
        return items
    }
    /**
     * 返回支持的處理器名稱
     */
    handlers(): Array<string> {
        return this.handler.names
    }
}
const _Core = new Core()
/**
 *  返回默認的 系統
 */
export function Background(): Core {
    return _Core
}
/**
 * 使用 要使用的 型別
 * @param pkg 包名
 * @param name 型別名
 */
export function UseType(pkg: string, name: string, repeat?: boolean): Type {
    return new Type(pkg, name, repeat)
}

export class CoreTypes {
    readonly int16 = new Type("core", "int16")
    readonly int32 = new Type("core", "int32")
    readonly int64 = new Type("core", "int64")
    readonly uint16 = new Type("core", "uint16")
    readonly uint32 = new Type("core", "uint32")
    readonly uint64 = new Type("core", "uint64")
    readonly float32 = new Type("core", "float32")
    readonly float64 = new Type("core", "float64")
    readonly byte = new Type("core", "byte")
    readonly bool = new Type("core", "bool")
    readonly string = new Type("core", "string")

    readonly int16s = new Type("core", "int16", true)
    readonly int32s = new Type("core", "int32", true)
    readonly int64s = new Type("core", "int64", true)
    readonly uint16s = new Type("core", "uint16", true)
    readonly uint32s = new Type("core", "uint32", true)
    readonly uint64s = new Type("core", "uint64", true)
    readonly float32s = new Type("core", "float32", true)
    readonly float64s = new Type("core", "float64", true)
    readonly bytes = new Type("core", "byte", true)
    readonly bools = new Type("core", "bool", true)
    readonly strings = new Type("core", "string", true)

    isCore(t: Type): boolean {
        let ok = false
        switch (t) {
            case this.int16:
            case this.int32:
            case this.int64:
            case this.uint16:
            case this.uint32:
            case this.uint64:
            case this.float32:
            case this.float64:
            case this.byte:
            case this.bool:
            case this.string:
            case this.int16s:
            case this.int32s:
            case this.int64s:
            case this.uint16s:
            case this.uint32s:
            case this.uint64s:
            case this.float32s:
            case this.float64s:
            case this.bytes:
            case this.bools:
            case this.strings:
                ok = true
                break
        }
        return ok
    }
}
export const Types = new CoreTypes()