import { isString, isNullOrUndefined, isArray } from "util";
import { basename } from "path";
export class Type {
    private pkg_: string
    /**
     * 所在 包名
     */
    get pkg(): string {
        return this.pkg_
    }
    private name_: string
    /**
     * 型別名稱
     */
    get name(): string {
        return this.name_
    }
    private repeat_: boolean
    /**
     * 是否是數組
     */
    get repeat(): boolean {
        return this.repeat_
    }
    constructor(pkg: string, name: string, repeat?: boolean) {
        this.pkg_ = pkg
        this.name_ = name
        if (repeat) {
            this.repeat_ = repeat
        }
    }
}
export interface IField {
    id: number
    name: string
    type: Type
}
/**
 * 一個 可編解碼的 消息
 */
export class Message {
    private pkg_: Package
    /**
     * 消息 所在 包
     */
    get pkg(): Package {
        return this.pkg_
    }
    /**
     * 消息 所在 包名
     */
    get pkgName(): string {
        return this.pkg_.name
    }
    private fileName_: string
    /**
    * 消息 所在 檔案名
    */
    get fileName(): string {
        return this.fileName_
    }
    private name_: string
    /**
     * 消息 類型名稱
     */
    get name(): string {
        return this.name_
    }
    private fields_: Array<IField>
    get fields(): Array<IField> {
        return this.fields_
    }
    private keysID_: Map<number, IField>
    private keysName_: Map<string, IField>
    constructor(pkg: Package, name: string, fileName: string) {
        this.pkg_ = pkg
        this.name_ = name
        this.keysID_ = new Map<number, IField>()
        this.keysName_ = new Map<string, IField>()
        this.fields_ = new Array<IField>()
        this.fileName_ = fileName
    }
    register(field: IField | Array<IField>): Message {
        if (isArray(field)) {
            const items = field as Array<IField>
            for (let i = 0; i < items.length; i++) {
                this._register(items[i])
            }
        } else {
            this._register(field as IField)
        }
        return this
    }
    private _register(field: IField) {
        field.id = Math.floor(field.id)
        if (field.id < 1) {
            throw new Error("field.id not support < 1")
        }
        const name = field.name.trim()
        if (0 == name.length) {
            throw new Error("field.name not support empty")
        }
        if (isNullOrUndefined(field.type)) {
            throw new Error("field.type not support null")
        }

        if (this.keysID_.has(field.id)) {
            throw new Error("field.id already exists," + field.id)
        }
        if (this.keysName_.has(field.name)) {
            throw new Error("field.name already exists," + field.name)
        }

        this.keysID_.set(field.id, field)
        this.keysName_.set(field.name, field)
        this.fields_.push(field)
    }
}
/**
 * 包信息
 */
export class Package {
    private name_: string
    /**
     * 返回 包名
     */
    get name(): string {
        return this.name_
    }

    private keys_: Map<string, Message>
    private items_: Map<string, Array<Message>>
    /**
     * 包中 註冊的 消息
     */
    get keys(): Map<string, Message> {
        return this.keys_
    }
    /**
     * 包中 註冊的 消息
     */
    get items(): Map<string, Array<Message>> {
        return this.items_
    }

    constructor(name: string) {
        this.name_ = name
        this.keys_ = new Map<string, Message>()
        this.items_ = new Map<string, Array<Message>>()
    }
    /**
     * 註冊消息
     * @param name 消息名
     * @param filename 檔案名
     */
    register(name: string, filename: string): Message {
        if (!isString(name)) {
            throw new Error("invalid parameter name,only support string")
        }
        if (name.length == 0) {
            throw new Error("invalid parameter name,not support empty")
        }
        // 查找 消息
        if (this.keys_.has(name)) {
            throw new Error("message already exists," + this.name_ + "." + name)
        }

        // 註冊 消息
        filename = basename(filename, ".js")
        const msg = new Message(this, name, filename)
        this.keys_.set(name, msg)

        let items = this.items_.get(filename)
        if (!items) {
            items = new Array<Message>()
            this.items_.set(filename, items)
        }
        items.push(msg)
        return msg
    }
}
/**
 * 上下文信息
 */
export class Context {
    /**
     * 註冊的 包
     */
    keys = new Map<string, Package>()

    /**
     * 註冊一個消息
     * @param pkg 包名
     * @param name 消息名
     * @param filename 檔案名
     */
    register(pkg: string, name: string, filename: string): Message {
        if (!isString(pkg)) {
            throw new Error("invalid parameter pkg,only support string")
        }
        pkg = pkg.trim()
        name = name.trim()
        if (pkg.length == 0) {
            throw new Error("invalid parameter pkg,not support empty")
        }
        // 查找 包
        let p = this.keys.get(pkg) as Package
        if (!p) {
            p = new Package(pkg)
            this.keys.set(pkg, p)
        }
        return p.register(name, filename)
    }
    /**
     * 打印 註冊 符號信息
     */
    display() {
        this.keys.forEach((pkg: Package) => {
            console.log("package :", pkg.name)
            pkg.keys.forEach((msg: Message) => {
                console.log("    msg :", msg.name, "{")
                msg.fields.forEach((field: IField) => {
                    if (field.type.repeat) {
                        console.log("            ", field.id, field.name, "[ repeat", field.type.pkg + "." + field.type.name, "]")
                    } else {
                        console.log("            ", field.id, field.name, "[", field.type.pkg + "." + field.type.name, "]")
                    }
                })
                console.log("          }")
            })
        })
    }
    /**
     * 返回指定 消息的 符號信息
     * @param pkg 包名
     * @param name 類型名
     */
    getMessage(pkg: string, name: string): Message {
        const p = this.keys.get(pkg)
        if (!p) {
            throw new Error("unknow package : " + pkg)
        }
        const msg = p.keys.get(name)
        if (!msg) {
            throw new Error("unknow message : " + pkg + "." + name)
        }
        return msg as Message
    }
}