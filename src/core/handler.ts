import { Context } from './type';
import { stat, Stats, unlink, readdir, rmdir } from 'fs';
import { join } from 'path';
import { WriteStream } from 'fs';
/**
 * 處理接口
 */
export interface Handler {
    /**
     * 處理器 唯一識 名稱
     */
    readonly name: string

    /**
     * 
     * @param ctx 上下文信息
     * @param dst 輸出檔案夾
     */
    handle(ctx: Context, dst: string): Promise<void>
}
/**
 * 處理器
 */
export class Handlers {
    private handlers_ = new Map<string, Handler>()
    private names_ = new Array<string>()
    /**
     * 返回所有處理器名稱
     */
    get names(): Array<string> {
        return this.names_
    }
    /**
     * 註冊新的 處理器
     * @param handler 處理器 接口
     */
    register(handler: Handler) {
        if (this.handlers_.has(handler.name)) {
            throw new Error("handler already exists : " + handler.name)
        }
        this.handlers_.set(handler.name, handler)
        this.names_.push(handler.name)
    }
    /**
     * 調用處理器 創建代碼
     * @param ctx 註冊符號上下文
     * @param name 處理器名稱
     * @param dst 輸出目錄
     */
    handle(ctx: Context, name: string, dst: string): Promise<void> {
        const handler = this.handlers_.get(name)
        if (!handler) {
            throw new Error("handler not exists : " + name)
        }
        return handler.handle(ctx, dst)
    }
}
/**
 * 定義一個基類 爲Handler 提供一些常用 方法
 */
export class HandlerHelper {
    /**
     * 刪除 檔案/檔案夾
     * @param src 要刪除的路徑
     */
    async rmAll(file: string): Promise<void> {
        const stats: Stats = await new Promise((resolve, reject) => {
            stat(file, (e: NodeJS.ErrnoException | null, stats: Stats) => {
                if (e == null) {
                    resolve(stats)
                } else {
                    if (e.code === 'ENOENT') {
                        resolve()
                    } else {
                        reject(e)
                    }
                }
            })
        })
        if (!stats) {
            return
        }
        if (stats.isDirectory()) {
            await this._rmDir(file)
        } else {
            await this.rmFile(file)
        }
    }
    private async _rmDir(file: string): Promise<void> {
        const strs: string[] = await new Promise((resolve, reject) => {
            readdir(file, { encoding: "utf8" }, (e: Error | null, files: string[]) => {
                if (e) {
                    reject(e)
                } else {
                    resolve(files)
                }
            })
        })
        if (strs) {
            for (let i = 0; i < strs.length; i++) {
                await this.rmAll(join(file, strs[i]))
            }
        }
        return new Promise((resolve, reject) => {
            rmdir(file, (e: Error | null) => {
                if (e) {
                    reject(e)
                } else {
                    resolve()
                }
            })
        })
    }
    rmFile(file: string): Promise<void> {
        return new Promise((resolve, reject) => {
            unlink(file, (e: Error | null) => {
                if (e) {
                    reject(e)
                } else {
                    resolve()
                }
            })
        })
    }
}
export const DefaultHandlers = new Handlers()
export function writeString(w: WriteStream, str: string): Promise<void> {
    return new Promise((resolve, reject) => {
        w.write(str, "utf8", (e) => {
            if (e) {
                reject(e)
            } else {
                resolve()
            }
        })
    })
}