import { join } from 'path';
import { mkdir } from 'mkdir-recursive';
import { Context, Package, Message, IField, Type, } from '../../core/type';
import { Types, } from '../../core/core';
import { DefaultHandlers, HandlerHelper, writeString } from '../../core/handler';
import { createWriteStream, WriteStream } from 'fs';
import { types } from 'util';
import { type } from 'os';
function getClassName(pkg: string, name: string): string {
    return pkg.toLowerCase().replace("/", "_") + "_" + name.toLowerCase()
}
function getFieldName(f: IField): string {
    return f.name.toLowerCase()
}
function getTypeName(t: Type): string {
    let name: string
    switch (t) {
        case Types.int16:
            name = "int16_t"
            break
        case Types.int32:
            name = "int32_t"
            break
        case Types.int64:
            name = "int64_t"
            break

        case Types.uint16:
            name = "uint16_t"
            break
        case Types.uint32:
            name = "uint32_t"
            break
        case Types.uint64:
            name = "uint64_t"
            break

        case Types.float32:
            name = "float"
            break
        case Types.float64:
            name = "double"
            break

        case Types.byte:
            name = "uint8_t"
            break
        case Types.bool:
            name = "uint8_t"
            break
        case Types.string:
            name = "binary_c_string_t"
            break

        case Types.int16s:
        case Types.int32s:
        case Types.int64s:
        case Types.uint16s:
        case Types.uint32s:
        case Types.uint64s:
        case Types.float32s:
        case Types.float64s:
        case Types.bytes:
        case Types.bools:
        case Types.strings:
            name = "binary_c_array_t"
            break
        default:
            if (t.repeat) {
                name = "binary_c_array_t"
            } else {
                name = getClassName(t.pkg, t.name)
                name = `binary_c_${name}_pt`
            }
    }
    return name
}
function getTypeVarName(t: Type): string {
    let name: string
    switch (t) {
        case Types.int16:
        case Types.int16s:
            name = "int16_t"
            break
        case Types.int32:
        case Types.int32s:
            name = "int32_t"
            break
        case Types.int64:
        case Types.int64s:
            name = "int64_t"
            break

        case Types.uint16:
        case Types.uint16s:
            name = "uint16_t"
            break
        case Types.uint32:
        case Types.uint32s:
            name = "uint32_t"
            break
        case Types.uint64:
        case Types.uint64s:
            name = "uint64_t"
            break

        case Types.float32:
        case Types.float32s:
            name = "float"
            break
        case Types.float64:
        case Types.float64s:
            name = "double"
            break

        case Types.byte:
        case Types.bytes:
            name = "uint8_t"
            break
        case Types.bool:
        case Types.bools:
            name = "uint8_t"
            break
        case Types.string:
        case Types.strings:
            name = "binary_c_string_t"
            break

        default:
            name = getClassName(t.pkg, t.name)
            name = `binary_c_${name}_pt`
    }
    return name
}

class Helper {
    private keysHeader = new Set<string>()
    ctx: Context
    /**
     * 輸出位置
     */
    dst: string
    /**
     * 包名
     */
    pkg: string
    /**
     * 檔案名
     */
    name: string
    /**
     * 消息
     */
    msg: Array<Message>
    constructor(ctx: Context, dst: string, pkg: string, name: string, msg: Array<Message>) {
        this.ctx = ctx
        this.dst = dst
        this.pkg = pkg
        this.name = name
        this.msg = msg
    }

    async writeH() {
        const w = createWriteStream(join(this.dst, this.pkg, this.name + ".h"),
            {
                encoding: "utf8",
                autoClose: true,
            },
        )
        const pkg = this.pkg.toUpperCase().replace("/", "_")
        await writeString(w, `#ifndef __BINARY_C_PROTOCOL_${pkg}_H__
#define __BINARY_C_PROTOCOL_${pkg}_H__
#include <binary_c.h>
`)
        // include
        for (let i = 0; i < this.msg.length; i++) {
            await this._writeHInclude(w, this.msg[i])
        }

        await writeString(w, `#ifdef __cplusplus
extern "C"
{
#endif

`)
        for (let i = 0; i < this.msg.length; i++) {
            await this._writeHMessage(w, this.msg[i])
        }
        await writeString(w, `#ifdef __cplusplus
}
#endif

#endif // __BINARY_C_PROTOCOL_${pkg}_H__`)
        w.close()
    }
    private async _writeHInclude(w: WriteStream, msg: Message) {
        for (let i = 0; i < msg.fields.length; i++) {
            const field = msg.fields[i]
            if (Types.isCore(field.type)) {
                continue
            }
            const find = this.ctx.getMessage(field.type.pkg, field.type.name)
            const str = join(find.pkgName, find.fileName + ".h")
            if (this.keysHeader.has(str)) {
                continue
            }
            this.keysHeader.add(str)
            await writeString(w, `#include <${str}>
`)
        }
    }
    private async _writeHMessage(w: WriteStream, msg: Message) {
        const className = getClassName(this.pkg, msg.name)
        await writeString(w, `        typedef struct
        {
`)

        for (let i = 0; i < msg.fields.length; i++) {
            await this._writeHField(w, msg, msg.fields[i])
        }

        await writeString(w, `        } binary_c_${className}_t, *binary_c_${className}_pt;
        void binary_c_${className}_reset(binary_c_${className}_pt msg);
        void binary_c_${className}_free(binary_c_${className}_pt msg);
        int binary_c_${className}_marshal(uint8_t **output, int n, binary_c_${className}_pt msg);
        int binary_c_${className}_unmarshal(uint8_t *input, int n, binary_c_${className}_pt msg);

`)

        // set get
        for (let i = 0; i < msg.fields.length; i++) {
            if (msg.fields[i].type.repeat) {
                await this._writeHFieldSetGet(w, msg, msg.fields[i])
            }
        }

    }
    private async _writeHFieldSetGet(w: WriteStream, msg: Message, field: IField) {
        const className = getClassName(this.pkg, msg.name)
        const name = field.name.toLowerCase()
        const t = getTypeVarName(field.type)
        await writeString(w, `        void binary_c_${className}_${name}_set(binary_c_${className}_pt msg,int i,${t} data);
        ${t} binary_c_${className}_${name}_get(const binary_c_${className}_pt msg,int i);
`)
    }
    private async _writeHField(w: WriteStream, msg: Message, field: IField) {
        const fieldName = getFieldName(field)
        const typename = getTypeName(field.type)
        await writeString(w, `                ${typename} ${fieldName}; // ${field.id};
`)
    }


    private async _writeCResetField(w: WriteStream, msg: Message, field: IField) {
        const fieldName = getFieldName(field)
        switch (field.type) {
            case Types.int16:
            case Types.int32:
            case Types.int64:
            case Types.uint16:
            case Types.uint32:
            case Types.uint64:
            case Types.float32:
            case Types.float64:
            case Types.byte:
            case Types.bool:
                await writeString(w, `   msg->${fieldName} = 0;
`)
                break
            case Types.string:
                await writeString(w, `   msg->${fieldName}.data = NULL;
   msg->${fieldName}.length = 0;
   msg->${fieldName}.capacity = 0;
`)
                break
            default:
                if (field.type.repeat) {
                    await writeString(w, `   msg->${fieldName}.data = NULL;
   msg->${fieldName}.length = 0;
   msg->${fieldName}.capacity = 0;
`)
                } else {
                    await writeString(w, `   msg->${fieldName} = NULL;
`)
                }
        }
    }
    private async _writeCReset(w: WriteStream, msg: Message) {
        const className = getClassName(this.pkg, msg.name)
        await writeString(w, `void binary_c_${className}_reset(binary_c_${className}_pt msg)
{
`)
        for (let i = 0; i < msg.fields.length; i++) {
            await this._writeCResetField(w, msg, msg.fields[i])
        }
        await writeString(w, "}\n\n")
    }
    private async _writeCFreeField(w: WriteStream, msg: Message, field: IField) {
        const fieldName = getFieldName(field)
        switch (field.type) {
            case Types.int16:
            case Types.int32:
            case Types.int64:
            case Types.uint16:
            case Types.uint32:
            case Types.uint64:
            case Types.float32:
            case Types.float64:
            case Types.byte:
            case Types.bool:
                await writeString(w, `   msg->${fieldName} = 0;
`)
                break
            case Types.string:
                await writeString(w, `   if(msg->${fieldName}.capacity)
   {
      cnf->free(msg->${fieldName}.data);
   }
   msg->${fieldName}.data = NULL;
   msg->${fieldName}.length = 0;
   msg->${fieldName}.capacity = 0;
`)
                break
            default:
                if (field.type.repeat) {
                    const name = field.name.toLowerCase()
                    if (Types.strings == field.type) {
                        await writeString(w, `   binary_c_free_strings(&msg->${name});
`);
                    } else if (!Types.isCore(field.type)) {
                        const className = getClassName(field.type.pkg, field.type.name)
                        const t = getTypeVarName(field.type)
                        await writeString(w, `   for(int i=0;i<msg->${fieldName}.length;i++)
   {
     binary_c_${className}_free(((${t}*)(msg->${name}.data))[i]);
   }
`)
                    }
                    await writeString(w, `   if(msg->${fieldName}.capacity)
   {
`)
                    await writeString(w, `      cnf->free(msg->${fieldName}.data);
   }
   msg->${fieldName}.data = NULL;
   msg->${fieldName}.length = 0;
   msg->${fieldName}.capacity = 0;
`)
                } else {
                    const className = getClassName(field.type.pkg, field.type.name)
                    await writeString(w, `   if(msg->${fieldName})
   {
      binary_c_${className}_free(msg->${fieldName});
      cnf->free(msg->${fieldName});
   }
   msg->${fieldName} = NULL;
`)
                }
        }
    }
    private async _writeCFree(w: WriteStream, msg: Message) {
        const className = getClassName(this.pkg, msg.name)
        await writeString(w, `void binary_c_${className}_free(binary_c_${className}_pt msg)
{
    binary_c_configure_pt cnf = binary_c_configure();
`)
        for (let i = 0; i < msg.fields.length; i++) {
            await this._writeCFreeField(w, msg, msg.fields[i])
        }
        await writeString(w, "}\n\n")
    }

    private async _writeCSetGetField(w: WriteStream, msg: Message, field: IField) {
        const className = getClassName(this.pkg, msg.name)
        const name = field.name.toLowerCase()
        const t = getTypeVarName(field.type)
        await writeString(w, `void binary_c_${className}_${name}_set(binary_c_${className}_pt msg,int i,${t} data)
{
    ((${t}*)(msg->${name}.data))[i] = data;
}
${t} binary_c_${className}_${name}_get(const binary_c_${className}_pt msg,int i)
{
    return ((${t}*)(msg->${name}.data))[i];
}

`)
    }
    private async _writeCSetGet(w: WriteStream, msg: Message) {
        for (let i = 0; i < msg.fields.length; i++) {
            if (msg.fields[i].type.repeat) {
                await this._writeCSetGetField(w, msg, msg.fields[i])
            }
        }
    }
    async writeC() {
        const w = createWriteStream(join(this.dst, this.pkg, this.name + ".c"),
            {
                encoding: "utf8",
                autoClose: true,
            },
        )
        await writeString(w, `#include "${this.name}.h"

`)
        // reset
        for (let i = 0; i < this.msg.length; i++) {
            await this._writeCReset(w, this.msg[i])
        }
        // free
        for (let i = 0; i < this.msg.length; i++) {
            await this._writeCFree(w, this.msg[i])
        }
        // set/get 
        for (let i = 0; i < this.msg.length; i++) {
            await this._writeCSetGet(w, this.msg[i])
        }

        w.close()
    }

}

/**
 * 爲生成c代碼 創建的 處理器
 */
export class Handler extends HandlerHelper {
    readonly name = "c"
    async handle(ctx: Context, dst: string) {
        await this.rmAll(dst)
        if (ctx.keys.size == 0) {
            return
        }
        const pkgs = new Array<Package>()
        ctx.keys.forEach((pkg: Package) => {
            pkgs.push(pkg)
        })
        for (let i = 0; i < pkgs.length; i++) {
            const pkg = pkgs[i]
            await this._package(ctx, dst, pkg)
        }
    }
    private async _package(ctx: Context, dst: string, pkg: Package) {
        await new Promise((resolve, reject) => {
            mkdir(join(dst, pkg.name),
                0o755,
                (e: Error | null) => {
                    if (e) {
                        reject(e)
                    } else {
                        resolve()
                    }
                })
        })
        const iterator = pkg.items.entries()
        for (const v of iterator) {
            await this._msg(ctx, dst, pkg.name, v[0], v[1]);
        }
    }
    private async _msg(ctx: Context, dst: string, pkg: string, name: string, msg: Array<Message>) {
        const helper = new Helper(ctx, dst, pkg, name, msg)
        await helper.writeH()
        await helper.writeC()
    }
}

DefaultHandlers.register(new Handler())