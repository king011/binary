import { Background, UseType, Types } from '../../core/core';


const ctx = Background()
const pkg = "types"

// import 其它包
const pkgZooAnimal = "zoo/animal"

// 註冊 types 消息
ctx.register(pkg, "types", __filename).register([
    {
        id: 1,
        name: "int16",
        type: Types.int16,
    },
    {
        id: 2,
        name: "int32",
        type: Types.int32,
    },
    {
        id: 3,
        name: "int64",
        type: Types.int64,
    },

    {
        id: 4,
        name: "uint16",
        type: Types.uint16,
    },
    {
        id: 5,
        name: "uint32",
        type: Types.uint32,
    },
    {
        id: 6,
        name: "uint64",
        type: Types.uint64,
    },

    {
        id: 7,
        name: "float32",
        type: Types.float32,
    },
    {
        id: 8,
        name: "float64",
        type: Types.float64,
    },

    {
        id: 9,
        name: "b",
        type: Types.byte,
    },
    {
        id: 10,
        name: "ok",
        type: Types.bool,
    },
    {
        id: 11,
        name: "str",
        type: Types.string,
    },
    {
        id: 12,
        name: "cat",
        type: UseType(pkgZooAnimal, "cat"),
    },
    {
        id: 13,
        name: "dog",
        type: UseType(pkgZooAnimal, "dog"),
    },

    {
        id: 21,
        name: "int16s",
        type: Types.int16s,
    },
    {
        id: 22,
        name: "int32s",
        type: Types.int32s,
    },
    {
        id: 23,
        name: "int64s",
        type: Types.int64s,
    },

    {
        id: 24,
        name: "uint16s",
        type: Types.uint16s,
    },
    {
        id: 25,
        name: "uint32s",
        type: Types.uint32s,
    },
    {
        id: 26,
        name: "uint64s",
        type: Types.uint64s,
    },

    {
        id: 27,
        name: "float32s",
        type: Types.float32s,
    },
    {
        id: 28,
        name: "float64s",
        type: Types.float64s,
    },

    {
        id: 29,
        name: "bs",
        type: Types.bytes,
    },
    {
        id: 30,
        name: "oks",
        type: Types.bools,
    },
    {
        id: 31,
        name: "strs",
        type: Types.strings,
    },
    {
        id: 32,
        name: "cats",
        type: UseType(pkgZooAnimal, "cat", true),
    },
    {
        id: 33,
        name: "dogs",
        type: UseType(pkgZooAnimal, "dog", true),
    },
])

