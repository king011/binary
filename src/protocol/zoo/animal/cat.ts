import { Background, UseType, Types } from '../../../core/core';


const ctx = Background()
const pkg = "zoo/animal"

// 註冊 cat 消息
ctx.register(pkg, "cat", __filename).register([
    {
        id: 1,
        name: "name",
        type: Types.string,
    },
])
// 註冊 dog 消息
ctx.register(pkg, "dog", __filename).register([
    {
        id: 1,
        name: "id",
        type: Types.int64,
    },
    {
        id: 2,
        name: "eat",
        type: Types.strings,
    },
])

