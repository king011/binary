import { join } from 'path';
import { Background } from './core/core';

async function run() {
    try {
        const ctx = Background()
        await ctx.require(join(__dirname, "protocol"))
        ctx.display()
        await ctx.output("c", "/home/king/project/c/binary-c/src/protocol")
    } catch (e) {
        console.log(e)
    }
}
run()

